package com.tecso.ejercio1.service;

import com.tecso.ejercio1.dto.TitularDto;
import com.tecso.ejercio1.dto.TitularDto;
import com.tecso.ejercio1.model.Titular;

import java.util.List;

public interface TitularService {

    public abstract TitularDto addTitular(TitularDto juridicaDto);

    public abstract List<TitularDto> listTitulares();

    public abstract void removeTitular(Long cuid);
    
    public abstract TitularDto findTitular(Long cuid);

    public abstract Titular findTitularById(Long cuid);

    public abstract TitularDto updateTitular(TitularDto juridicaDto);
}
