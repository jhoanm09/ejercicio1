package com.tecso.ejercio1.service;

import com.tecso.ejercio1.dto.JuridicaDto;
import com.tecso.ejercio1.dto.TitularDto;
import com.tecso.ejercio1.model.Juridica;
import com.tecso.ejercio1.model.Titular;

import java.lang.management.ThreadInfo;
import java.util.List;

public interface JuridicaService {

    public abstract JuridicaDto addJuridicas(JuridicaDto juridicaDto);

    public abstract List<JuridicaDto> listJuridicas();

    public abstract void removeJuridica(TitularDto titularDto);
}
