package com.tecso.ejercio1.service.impl;

import com.tecso.ejercio1.converter.FisicaConverter;
import com.tecso.ejercio1.converter.JuridicaConverter;
import com.tecso.ejercio1.converter.TitularConverter;
import com.tecso.ejercio1.converter.TitularConverter;
import com.tecso.ejercio1.dto.TitularDto;
import com.tecso.ejercio1.dto.TitularDto;
import com.tecso.ejercio1.model.Fisica;
import com.tecso.ejercio1.model.Juridica;
import com.tecso.ejercio1.model.Titular;
import com.tecso.ejercio1.model.Titular;
import com.tecso.ejercio1.repository.TitularRepository;
import com.tecso.ejercio1.repository.TitularRepository;
import com.tecso.ejercio1.service.TitularService;
import com.tecso.ejercio1.service.TitularService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("titularServiceImpl")
public class TitularServiceImpl implements TitularService {

    @Autowired
    @Qualifier("titularRepository")
    private TitularRepository titularRepository;

    @Autowired
    @Qualifier("titularConverter")
    private TitularConverter titularConverter;

    @Autowired
    @Qualifier("fisicaConverter")
    private FisicaConverter fisicaConverter;

    @Autowired
    @Qualifier("juridicaConverter")
    private JuridicaConverter juridicaConverter;


    @Override
    public TitularDto addTitular(TitularDto titularDto) {
        Titular titular = titularRepository.save(titularConverter.convertTitularDto2Titular(titularDto));
        return titularConverter.convertTitular2TitularDto(titular);
    }

    @Override
    public List<TitularDto> listTitulares() {
        return  titularRepository.
                findAll().
                stream().
                map( j -> titularConverter.convertTitular2TitularDto( j )).
                collect( Collectors.toList());
    }

    @Override
    public void removeTitular(Long cuid) {
        Titular titular = findTitularById(cuid);
        if(titular != null){
            titularRepository.delete(titular);
        }
    }

    @Override
    public TitularDto findTitular(Long cuid) {
        return titularConverter.convertTitular2TitularDto( titularRepository.findByCuid( cuid ));
    }

    @Override
    public Titular findTitularById(Long cuid) {
        return titularRepository.findByCuid(cuid);
    }

    @Override
    public TitularDto updateTitular(TitularDto titularDto) {
        Titular titular = findTitularById(titularDto.getCuid());
        if (titularDto.getJuridicaDto() != null){
            Juridica juridica = titular.getJuridica();
            juridica.setRazonSocial(titularDto.getJuridicaDto().getRazonSocial());
            juridica.setAnioFundacion(titularDto.getJuridicaDto().getAnioFundacion());
            titular.setJuridica(juridica);
        }
        if (titularDto.getFisicaDto() != null){
            Fisica fisica = titular.getFisica();
            fisica.setNombre(titularDto.getFisicaDto().getNombre());
            fisica.setDni(titularDto.getFisicaDto().getDni());
            fisica.setApellido(titularDto.getFisicaDto().getApellido());
            titular.setFisica(fisica);
        }
        return titularConverter.convertTitular2TitularDto(titularRepository.saveAndFlush(titular));
    }
}
