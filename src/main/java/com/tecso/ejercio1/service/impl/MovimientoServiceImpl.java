package com.tecso.ejercio1.service.impl;

import com.tecso.ejercio1.converter.CuentaConverter;
import com.tecso.ejercio1.converter.MovimientoConverter;
import com.tecso.ejercio1.dto.CuentaDto;
import com.tecso.ejercio1.dto.MovimientoDto;
import com.tecso.ejercio1.model.Cuenta;
import com.tecso.ejercio1.model.Movimiento;
import com.tecso.ejercio1.repository.CuentaRepository;
import com.tecso.ejercio1.repository.MovimientoRepository;
import com.tecso.ejercio1.service.CuentaService;
import com.tecso.ejercio1.service.MovimientoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("movimientoServiceImpl")
public class MovimientoServiceImpl implements MovimientoService {

    @Autowired
    @Qualifier("movimientoRepository")
    private MovimientoRepository movimientoRepository;

    @Autowired
    @Qualifier("movimientoConverter")
    private MovimientoConverter movimientoConverter;

    @Autowired
    @Qualifier("cuentaServiceImpl")
    private CuentaServiceImpl cuentaService;

    @Override
    public MovimientoDto addMovimiento( MovimientoDto movimientoDto ) {
        Movimiento movimiento = movimientoConverter.convertMovimientoDto2Movimiento(movimientoDto);
        movimiento.setCuenta(cuentaService.findCuentaById(movimientoDto.getCuentaDto().getNumeroCuenta()));
        movimientoRepository.save(movimiento);
        return movimientoConverter.convertMovimiento2MovimientoDto(movimiento);
    }

    @Override
    public void removeMovimiento( Long idMovimiento ) {
        Movimiento movimiento = findMovimientoById(idMovimiento);
        if(movimiento != null){
            movimientoRepository.delete( movimiento );
        }
    }

    @Override
    public Movimiento findMovimientoById(Long idMovimiento) {
        return movimientoRepository.findByIdMovimiento(idMovimiento);
    }

    @Override
    public List<MovimientoDto> listMovimientos() {
        return  movimientoRepository.
                findAll().
                stream().
                map( m -> movimientoConverter.convertMovimiento2MovimientoDto( m )).
                collect( Collectors.toList());
    }
}
