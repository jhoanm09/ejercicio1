package com.tecso.ejercio1.service.impl;

import com.tecso.ejercio1.converter.JuridicaConverter;
import com.tecso.ejercio1.converter.TitularConverter;
import com.tecso.ejercio1.dto.JuridicaDto;
import com.tecso.ejercio1.dto.TitularDto;
import com.tecso.ejercio1.model.Juridica;
import com.tecso.ejercio1.model.Titular;
import com.tecso.ejercio1.repository.JuridicaRepository;
import com.tecso.ejercio1.service.JuridicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("juridicaServiceImpl")
public class JuridicaServiceImpl implements JuridicaService {

    @Autowired
    @Qualifier("juridicaRepository")
    private JuridicaRepository juridicaRepository;

    @Autowired
    @Qualifier("juridicaConverter")
    private JuridicaConverter juridicaConverter;

    @Autowired
    @Qualifier("titularConverter")
    private TitularConverter titularConverter;

    @Override
    public JuridicaDto addJuridicas(JuridicaDto juridicaDto) {
        Juridica juridica = juridicaRepository.save(juridicaConverter.convertJuridicaDto2Juridica(juridicaDto));
        return juridicaConverter.convertJuridica2JuridicaDto(juridica);
    }

    @Override
    public void removeJuridica(TitularDto titularDto) {
        Juridica juridica = juridicaRepository.findByTitular(titularConverter.convertTitularDto2Titular( titularDto));
        juridicaRepository.delete( juridica );
    }

    @Override
    public List<JuridicaDto> listJuridicas() {
        return  juridicaRepository.
                findAll().
                stream().
                map( j -> juridicaConverter.convertJuridica2JuridicaDto( j )).
                collect( Collectors.toList());
    }

}
