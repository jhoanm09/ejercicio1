package com.tecso.ejercio1.service.impl;
import com.tecso.ejercio1.converter.CuentaConverter;
import com.tecso.ejercio1.dto.CuentaDto;
import com.tecso.ejercio1.model.Cuenta;
import com.tecso.ejercio1.repository.CuentaRepository;
import com.tecso.ejercio1.service.CuentaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service("cuentaServiceImpl")
public class CuentaServiceImpl implements CuentaService {

    @Autowired
    @Qualifier("cuentaRepository")
    private CuentaRepository cuentaRepository;

    @Autowired
    @Qualifier("cuentaConverter")
    private CuentaConverter cuentaConverter;

    @Autowired
    @Qualifier("titularServiceImpl")
    private TitularServiceImpl titularService;

    @Override
    public CuentaDto addCuenta(CuentaDto cuentaDto) {
        Cuenta cuenta = cuentaConverter.convertCuentaDto2Cuenta(cuentaDto);
        cuenta.setTitular(titularService.findTitularById(cuentaDto.getTitularDto().getCuid()));
        cuentaRepository.save(cuenta);
        return cuentaConverter.convertCuenta2CuentaDto(cuenta);
    }

    @Override
    public void removeCuenta(Cuenta cuenta) {
        if(cuenta != null){
            cuentaRepository.delete(cuenta);
        }
    }

    @Override
    public Cuenta findCuentaById(Long numeroCuenta) {
        return cuentaRepository.findByNumeroCuenta(numeroCuenta);
    }

    @Override
    public List<CuentaDto> listCuentas() {
        return  cuentaRepository.
                findAll().
                stream().
                map( j -> cuentaConverter.convertCuenta2CuentaDto( j )).
                collect( Collectors.toList());
    }

    @Override
    public List<CuentaDto> listCuentaMovimientos() {
        return cuentaRepository.
                findAll().
                stream().
                map( c -> cuentaConverter.convertCuentaMov2CuentaDtoMov( c )).
                collect( Collectors.toList());
    }
}
