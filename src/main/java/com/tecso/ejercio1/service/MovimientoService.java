package com.tecso.ejercio1.service;

import com.tecso.ejercio1.dto.CuentaDto;
import com.tecso.ejercio1.dto.MovimientoDto;
import com.tecso.ejercio1.model.Cuenta;
import com.tecso.ejercio1.model.Movimiento;
import com.tecso.ejercio1.model.Titular;

import java.util.List;

public interface MovimientoService {

    public abstract MovimientoDto addMovimiento(MovimientoDto movimientoDto);

    public abstract void removeMovimiento(Long numeroCuenta);

    public abstract Movimiento findMovimientoById(Long idMovimiento);

    public abstract List<MovimientoDto> listMovimientos();

}
