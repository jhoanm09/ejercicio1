package com.tecso.ejercio1.service;

import com.tecso.ejercio1.dto.CuentaDto;
import com.tecso.ejercio1.dto.TitularDto;
import com.tecso.ejercio1.model.Cuenta;
import com.tecso.ejercio1.model.Titular;

import java.util.List;

public interface CuentaService {

    public abstract CuentaDto addCuenta(CuentaDto cuentaDto);

    public abstract void removeCuenta(Cuenta cuenta);

    public abstract Cuenta findCuentaById(Long numeroCuenta);

    public abstract List<CuentaDto> listCuentas();

    public abstract List<CuentaDto> listCuentaMovimientos();
}
