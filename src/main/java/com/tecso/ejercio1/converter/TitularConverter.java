package com.tecso.ejercio1.converter;

import com.tecso.ejercio1.dto.JuridicaDto;
import com.tecso.ejercio1.dto.TitularDto;
import com.tecso.ejercio1.model.Juridica;
import com.tecso.ejercio1.model.Titular;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component("titularConverter")
public class TitularConverter {

    @Autowired
    @Qualifier("juridicaConverter")
    private JuridicaConverter juridicaConverter;

    @Autowired
    @Qualifier("fisicaConverter")
    private FisicaConverter fisicaConverter;

    public Titular convertTitularDto2Titular(TitularDto titularDto) {
        Titular titular = new Titular( );
        titular.setCuid( titularDto.getCuid() );
        if(titularDto.getJuridicaDto() != null){
            titular.setJuridica(juridicaConverter.convertJuridicaDto2Juridica(titularDto.getJuridicaDto()));
        }
        if(titularDto.getFisicaDto() != null){
            titular.setFisica(fisicaConverter.convertFisicaDto2Fisica(titularDto.getFisicaDto()));
        }
        return titular;
    }

    public TitularDto convertTitular2TitularDto(Titular titular) {
        TitularDto titularDto = new TitularDto( );
        titularDto.setCuid( titular.getCuid() );
        if(titular.getJuridica() != null){
            titularDto.setJuridicaDto(juridicaConverter.convertJuridica2JuridicaDto(titular.getJuridica()));
        }
        if(titular.getFisica() != null){
            titularDto.setFisicaDto(fisicaConverter.convertFisica2FisicaDto(titular.getFisica()));
        }
        return titularDto;
    }
}
