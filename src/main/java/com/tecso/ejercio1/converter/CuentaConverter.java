package com.tecso.ejercio1.converter;

import com.tecso.ejercio1.dto.CuentaDto;
import com.tecso.ejercio1.dto.MovimientoDto;
import com.tecso.ejercio1.dto.TitularDto;
import com.tecso.ejercio1.model.Cuenta;
import com.tecso.ejercio1.model.Titular;
import com.tecso.ejercio1.service.impl.JuridicaServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component("cuentaConverter")
public class CuentaConverter {

    @Autowired
    @Qualifier("titularConverter")
    private TitularConverter titularConverter;

    @Autowired
    @Qualifier("movimientoConverter")
    private MovimientoConverter movimientoConverter;

    public Cuenta convertCuentaDto2Cuenta(CuentaDto cuentaDto) {
        Cuenta cuenta= new Cuenta( );
        cuenta.setMoneda(cuentaDto.getMoneda());
        cuenta.setSaldo(cuentaDto.getSaldo());
        return cuenta;
    }

    public CuentaDto convertCuenta2CuentaDto(Cuenta cuenta) {
        CuentaDto cuentaDto= new CuentaDto( );
        cuentaDto.setMoneda(cuenta.getMoneda());
        cuentaDto.setSaldo(cuenta.getSaldo());
        cuentaDto.setTitularDto(titularConverter.convertTitular2TitularDto(cuenta.getTitular()));
        return cuentaDto;
    }

    public CuentaDto convertCuentaMov2CuentaDtoMov(Cuenta cuenta) {
        CuentaDto cuentaDto= new CuentaDto( );
        cuentaDto.setNumeroCuenta(cuenta.getNumeroCuenta());
        cuentaDto.setMoneda(cuenta.getMoneda());
        cuentaDto.setSaldo(cuenta.getSaldo());
        Set<MovimientoDto> movimientoDtos = cuenta.
                                            getMovimientos().
                                            stream().
                                            map( m -> movimientoConverter.convertMovimientoCuen2MovimientoDtoCuen( m )).
                                            collect( Collectors.toSet());
        cuentaDto.setMovimientoDtos(movimientoDtos);

        return cuentaDto;
    }
}
