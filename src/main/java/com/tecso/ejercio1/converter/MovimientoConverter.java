package com.tecso.ejercio1.converter;

import com.tecso.ejercio1.dto.CuentaDto;
import com.tecso.ejercio1.dto.MovimientoDto;
import com.tecso.ejercio1.model.Cuenta;
import com.tecso.ejercio1.model.Movimiento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.TimeZone;

@Component("movimientoConverter")
public class MovimientoConverter {

    @Autowired
    @Qualifier("cuentaConverter")
    private CuentaConverter cuentaConverter;

    SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");

    public Movimiento convertMovimientoDto2Movimiento(MovimientoDto movimientoDto) {
        Movimiento movimiento= new Movimiento( );
        movimiento.setDescripcion(movimientoDto.getDescripcion());
        movimiento.setImporte(movimientoDto.getImporte());
        movimiento.setTipoMovimiento(movimientoDto.getTipoMovimiento());
        return movimiento;
    }

    public MovimientoDto convertMovimiento2MovimientoDto(Movimiento movimiento) {
        MovimientoDto movimientoDto= new MovimientoDto( );
        movimientoDto.setDescripcion(movimiento.getDescripcion());
        movimientoDto.setImporte(movimiento.getImporte());
        movimientoDto.setTipoMovimiento(movimiento.getTipoMovimiento());
        movimientoDto.setFecha(movimiento.getFecha());
        movimientoDto.setCuentaDto(cuentaConverter.convertCuenta2CuentaDto(movimiento.getCuenta()));
        return movimientoDto;
    }

    public MovimientoDto convertMovimientoCuen2MovimientoDtoCuen(Movimiento movimiento) {
        MovimientoDto movimientoDto= new MovimientoDto( );
        movimientoDto.setDescripcion(movimiento.getDescripcion());
        movimientoDto.setImporte(movimiento.getImporte());
        movimientoDto.setTipoMovimiento(movimiento.getTipoMovimiento());
        movimientoDto.setFecha(movimiento.getFecha());
        return movimientoDto;
    }
}
