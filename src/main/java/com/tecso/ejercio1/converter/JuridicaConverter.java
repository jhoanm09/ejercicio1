package com.tecso.ejercio1.converter;

import com.tecso.ejercio1.dto.JuridicaDto;
import com.tecso.ejercio1.dto.TitularDto;
import com.tecso.ejercio1.model.Juridica;
import com.tecso.ejercio1.model.Titular;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component("juridicaConverter")
public class JuridicaConverter {

    @Autowired
    @Qualifier("titularConverter")
    private TitularConverter titularConverter;

    public Juridica convertJuridicaDto2Juridica(JuridicaDto juridicaDto) {
        Juridica juridica = new Juridica( );
        juridica.setAnioFundacion( juridicaDto.getAnioFundacion() );
        juridica.setRazonSocial( juridicaDto.getRazonSocial() );
        return juridica;
    }

    public JuridicaDto convertJuridica2JuridicaDto(Juridica juridica) {
        JuridicaDto juridicaDto = new JuridicaDto( );
        juridicaDto.setAnioFundacion( juridica.getAnioFundacion() );
        juridicaDto.setRazonSocial( juridica.getRazonSocial() );
        return juridicaDto;
    }
}
