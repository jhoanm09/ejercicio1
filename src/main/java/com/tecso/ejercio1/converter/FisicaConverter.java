package com.tecso.ejercio1.converter;

import com.tecso.ejercio1.dto.FisicaDto;
import com.tecso.ejercio1.dto.JuridicaDto;
import com.tecso.ejercio1.model.Fisica;
import com.tecso.ejercio1.model.Juridica;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component("fisicaConverter")
public class FisicaConverter {

    @Autowired
    @Qualifier("titularConverter")
    private TitularConverter titularConverter;

    public Fisica convertFisicaDto2Fisica(FisicaDto fisicaDto) {
        Fisica fisica = new Fisica( );
        fisica.setDni(fisicaDto.getDni());
        fisica.setApellido(fisicaDto.getApellido());
        fisica.setNombre(fisicaDto.getNombre());
        return fisica;
    }

    public FisicaDto convertFisica2FisicaDto(Fisica fisica) {
        FisicaDto fisicaDto = new FisicaDto( );
        fisicaDto.setDni(fisica.getDni());
        fisicaDto.setApellido(fisica.getApellido());
        fisicaDto.setNombre(fisica.getNombre());
        return fisicaDto;
    }
}
