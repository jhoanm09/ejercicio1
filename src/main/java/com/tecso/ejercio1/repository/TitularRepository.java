package com.tecso.ejercio1.repository;

import com.tecso.ejercio1.model.Juridica;
import com.tecso.ejercio1.model.Titular;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository("titularRepository")
public interface TitularRepository extends JpaRepository<Titular, Serializable> {

    public abstract Titular findByCuid(Long cuid);

}
