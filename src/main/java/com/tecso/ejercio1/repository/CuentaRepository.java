package com.tecso.ejercio1.repository;

import com.tecso.ejercio1.model.Cuenta;
import com.tecso.ejercio1.model.Titular;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository("cuentaRepository")
public interface CuentaRepository extends JpaRepository<Cuenta, Serializable> {

    public abstract Cuenta findByNumeroCuenta(Long numeroCuenta);
}
