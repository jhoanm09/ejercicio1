package com.tecso.ejercio1.repository;

import com.tecso.ejercio1.model.Movimiento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository("movimientoRepository")
public interface MovimientoRepository extends JpaRepository<Movimiento, Serializable> {

    public abstract Movimiento findByIdMovimiento(Long idMovimiento);
}
