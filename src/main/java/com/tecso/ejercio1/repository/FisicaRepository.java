package com.tecso.ejercio1.repository;

import com.tecso.ejercio1.model.Titular;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;

public interface FisicaRepository extends JpaRepository<Titular, Serializable> {

}
