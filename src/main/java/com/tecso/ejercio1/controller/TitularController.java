package com.tecso.ejercio1.controller;

import com.tecso.ejercio1.dto.JuridicaDto;
import com.tecso.ejercio1.dto.TitularDto;
import com.tecso.ejercio1.service.impl.JuridicaServiceImpl;
import com.tecso.ejercio1.service.impl.TitularServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/titularController")
public class TitularController {

    @Autowired
    @Qualifier("titularServiceImpl")
    private TitularServiceImpl titularService;

    @PostMapping(path = "/addTitular")
    public TitularDto addTitular(@RequestBody TitularDto titularDto) {
         return titularService.addTitular( titularDto );
    }

    @GetMapping(path = "/listTitulares")
    public ResponseEntity<List<TitularDto>> listTitulares() {
        return new ResponseEntity<List<TitularDto>>( titularService.listTitulares(), HttpStatus.OK );
    }

    @GetMapping(path = "/removeTitular")
    public ResponseEntity<List<TitularDto>> removeTitular(@RequestParam Long cuid) {
        titularService.removeTitular( cuid );
        return listTitulares();
    }

    @PostMapping(path = "/updateTitular")
    public TitularDto updateTitular(@RequestBody TitularDto titularDto) {
        return titularService.updateTitular( titularDto );
    }

}
