package com.tecso.ejercio1.controller;

import com.tecso.ejercio1.dto.CuentaDto;
import com.tecso.ejercio1.dto.MovimientoDto;
import com.tecso.ejercio1.model.Cuenta;
import com.tecso.ejercio1.service.impl.CuentaServiceImpl;
import com.tecso.ejercio1.service.impl.MovimientoServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/movimientoController")
public class MovimientoController {

    @Autowired
    @Qualifier("movimientoServiceImpl")
    private MovimientoServiceImpl movimientoService;

    @Autowired
    @Qualifier("cuentaServiceImpl")
    private CuentaServiceImpl cuentaService;



    @PostMapping(path = "/addMovimiento")
    public MovimientoDto addMovimiento(@RequestBody MovimientoDto movimientoDto) {
        Cuenta cuenta = cuentaService.findCuentaById(movimientoDto.getCuentaDto().getNumeroCuenta());
        if(cuenta.getMoneda().equals("peso") && (movimientoDto.getImporte().compareTo(new BigDecimal(1000)) == 1)){
            throw new IllegalArgumentException("No se puede generar el movimiento. Genera un descubierto de 1000");
        } else if(cuenta.getMoneda().equals("dolar") && (movimientoDto.getImporte().compareTo(new BigDecimal(300)) == 1)){
            throw new IllegalArgumentException("No se puede generar el movimiento. Genera un descubierto de 300");
        } else if(cuenta.getMoneda().equals("euro") && (movimientoDto.getImporte().compareTo(new BigDecimal(150)) == 1)){
            throw new IllegalArgumentException("No se puede generar el movimiento. Genera un descubierto de 150");
        }
        return movimientoService.addMovimiento( movimientoDto );
    }

    @GetMapping(path = "/removeMovimiento")
    public ResponseEntity<List<MovimientoDto>> removeMovimiento(@RequestParam Long idMovimiento) {
        movimientoService.removeMovimiento( idMovimiento );
        return listMovimientos();
    }

    @GetMapping(path = "/listMovimientos")
    public ResponseEntity<List<MovimientoDto>> listMovimientos() {
        return new ResponseEntity<List<MovimientoDto>>( movimientoService.listMovimientos(), HttpStatus.OK );
    }

}
