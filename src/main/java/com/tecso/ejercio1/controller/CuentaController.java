package com.tecso.ejercio1.controller;

import com.sun.xml.internal.ws.api.message.ExceptionHasMessage;
import com.tecso.ejercio1.dto.CuentaDto;
import com.tecso.ejercio1.dto.TitularDto;
import com.tecso.ejercio1.model.Cuenta;
import com.tecso.ejercio1.service.impl.CuentaServiceImpl;
import com.tecso.ejercio1.service.impl.TitularServiceImpl;
import org.hibernate.TransactionException;
import org.hibernate.internal.ExceptionMapperStandardImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cuentaController")
public class CuentaController {

    @Autowired
    @Qualifier("cuentaServiceImpl")
    private CuentaServiceImpl cuentaService;

    @PostMapping(path = "/addCuenta")
    public CuentaDto addTitular(@RequestBody CuentaDto cuentaDto) {
        try {
            return cuentaService.addCuenta( cuentaDto );
        }catch (TransactionSystemException e) {
            throw new RuntimeException("La moneda debe ser peso, euro o dolar. El saldo puede tener hasta dos digitos decimales", e);
        }
    }

    @GetMapping(path = "/listCuentas")
    public ResponseEntity<List<CuentaDto>> listCuentas() {
        return new ResponseEntity<List<CuentaDto>>( cuentaService.listCuentas(), HttpStatus.OK );
    }

    @GetMapping(path = "/removeCuenta")
    public ResponseEntity<List<CuentaDto>> removeCuenta(@RequestParam Long cuentaNumero) {
        Cuenta cuenta = cuentaService.findCuentaById(cuentaNumero);
        if(cuenta.getMovimientos() == null){
            cuentaService.removeCuenta( cuenta );
        } else {
            throw new IllegalArgumentException("La cuenta tiene movimientos, no puede ser eliminada");
        }
        return listCuentas();
    }

    @GetMapping(path = "/listCuentaMovimientos")
    public ResponseEntity<List<CuentaDto>> listCuentaMovimientos() {
        return new ResponseEntity<List<CuentaDto>>( cuentaService.listCuentaMovimientos(), HttpStatus.OK );
    }

}
