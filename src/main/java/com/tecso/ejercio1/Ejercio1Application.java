package com.tecso.ejercio1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ejercio1Application {

	public static void main(String[] args) {
		SpringApplication.run(Ejercio1Application.class, args);
	}

}
