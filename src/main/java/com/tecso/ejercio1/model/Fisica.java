package com.tecso.ejercio1.model;

import javax.persistence.*;

@Entity
@Table(name = "fisica")
public class Fisica {

    @Id
    @Column(name = "id_fisica")
    private Long idFisica;

    @Column(name = "dni", unique = true, nullable = false, length = 10)
    private Long dni;

    @Column(name = "nombre", nullable = false, length = 80)
    private String nombre;

    @Column(name = "apellido", nullable = false, length = 250)
    private String apellido;

    @MapsId
    @OneToOne(optional = false)
    private Titular titular;

    public Fisica() {
    }

    public Fisica(String nombre, String apellido) {
        this.nombre = nombre;
        this.apellido = apellido;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Long getDni() {
        return dni;
    }

    public void setDni(Long dni) {
        this.dni = dni;
    }

    public Titular getTitular() {
        return titular;
    }

    public void setTitular(Titular titular) {
        this.titular = titular;
    }
}
