package com.tecso.ejercio1.model;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "cuenta")
public class Cuenta {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "numero_Cuenta", unique = true, nullable = false, length = 10)
    private Long numeroCuenta;

    @Pattern(regexp = "^(euro|dolar|pesos)$", message = "La moneda debe de ser euro, dolar o peso")
    @Column(name = "moneda", nullable = false, length = 5)
    private String moneda;

    @Column(name = "saldo", nullable = false, scale = 2)
    private BigDecimal saldo;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Titular titular;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "cuenta")
    private Set<Movimiento> movimientos = new HashSet<>();

    public Cuenta() {
    }

    public Cuenta(String moneda, BigDecimal saldo) {
        this.moneda = moneda;
        this.saldo = saldo;
    }

    public Long getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(Long numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public Set<Movimiento> getMovimientos() {
        return movimientos;
    }

    public void setMovimientos(Set<Movimiento> movimientos) {
        this.movimientos = movimientos;
    }

    public Titular getTitular() {
        return titular;
    }

    public void setTitular(Titular titular) {
        this.titular = titular;
    }
}
