package com.tecso.ejercio1.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "titular")
public class Titular {

    @Id
    @Column(name = "cuid", unique = true, nullable = false, length = 10)
    private Long cuid;

    @OneToOne(mappedBy = "titular", cascade = CascadeType.ALL)
    private Juridica juridica;

    @OneToOne(mappedBy = "titular", cascade = CascadeType.ALL)
    private Fisica fisica;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "titular")
    private Set<Cuenta> cuentass = new HashSet<>();

    public Titular() {
    }

    public Titular(Long cuid, Juridica juridica) {
        this.cuid = cuid;
        this.juridica = juridica;
        this.juridica.setTitular(this);
    }

    public Titular(Long cuid) {
        this.cuid = cuid;
    }

    public Titular(Juridica juridica, Fisica fisica) {
        this.juridica = juridica;
        this.fisica = fisica;
    }

    public Long getCuid() {
        return cuid;
    }

    public void setCuid(Long cuid) {
        this.cuid = cuid;
    }

    public Juridica getJuridica() {
        return juridica;
    }

    public void setJuridica(Juridica juridica) {
        this.juridica = juridica;
        this.juridica.setTitular(this);
    }

    public Fisica getFisica() {
        return fisica;
    }

    public void setFisica(Fisica fisica) {
        this.fisica = fisica;
        this.fisica.setTitular(this);
    }
}
