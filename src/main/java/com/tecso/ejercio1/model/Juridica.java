package com.tecso.ejercio1.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "juridica")
public class Juridica {

    @Id
    @Column(name = "id_juridica")
    private Long idJuridica;

    @Column(name = "razon_social", nullable = false, length = 100)
    private String razonSocial;

    @Column(name = "anio_fundacion", nullable = false)
    private Date anioFundacion;

    @MapsId
    @OneToOne
    private Titular titular;

    public Juridica() {
    }

    public Juridica(String razosnSocial, Date anioFundacion) {
        this.razonSocial = razosnSocial;
        this.anioFundacion = anioFundacion;
    }

    public Long getIdJuridica() {
        return idJuridica;
    }

    public void setIdJuridica(Long idJuridica) {
        this.idJuridica = idJuridica;
    }

    public Date getAnioFundacion() {
        return anioFundacion;
    }

    public void setAnioFundacion(Date anioFundacion) {
        this.anioFundacion = anioFundacion;
    }

    public Titular getTitular() {
        return titular;
    }

    public void setTitular(Titular titular) {
        this.titular = titular;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }
}
