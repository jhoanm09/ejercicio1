package com.tecso.ejercio1.model;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;

@Entity
@Table(name = "movimiento")
public class Movimiento {

    @Id
    @GeneratedValue
    @Column(name = "id_movimiento", unique = true, nullable = false, length = 10)
    private Long idMovimiento;

    @Column(name = "fecha_movimiento", nullable = false)
    private ZonedDateTime fecha;

    @Pattern(regexp = "^(debito|credito)$" )
    @Column(name = "tipo_movimiento", nullable = false, length = 7)
    private String tipoMovimiento;

    @Column(name = "descripcion", nullable = false, length = 200)
    private String descripcion;

    @Column(name = "importe", nullable = false, scale = 2)
    private BigDecimal importe;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Cuenta cuenta;

    public Movimiento() {
        this.setFecha(ZonedDateTime.now( ZoneOffset.UTC ));
    }

    public Movimiento(ZonedDateTime fecha, @Pattern(regexp = "/(^debito$|^credito$)/") String tipoMovimiento, String descripcion, BigDecimal importe) {
        this.fecha = fecha;
        this.tipoMovimiento = tipoMovimiento;
        this.descripcion = descripcion;
        this.importe = importe;
    }

    public Long getIdMovimiento() {
        return idMovimiento;
    }

    public void setIdMovimiento(Long idMovimiento) {
        this.idMovimiento = idMovimiento;
    }

    public ZonedDateTime getFecha() {
        return fecha;
    }

    public void setFecha(ZonedDateTime fecha) {
        this.fecha = fecha;
    }

    public String getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(String tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }
}
