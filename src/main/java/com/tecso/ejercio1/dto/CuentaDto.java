package com.tecso.ejercio1.dto;

import com.tecso.ejercio1.model.Movimiento;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

public class CuentaDto {

    private Long numeroCuenta;

    private String moneda;

    private BigDecimal saldo;

    private TitularDto titularDto;

    private Set<MovimientoDto> movimientoDtos = new HashSet<>();

    public CuentaDto() {
    }

    public CuentaDto(String moneda, BigDecimal saldo) {
        this.moneda = moneda;
        this.saldo = saldo;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public TitularDto getTitularDto() {
        return titularDto;
    }

    public void setTitularDto(TitularDto titularDto) {
        this.titularDto = titularDto;
    }

    public Long getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(Long numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public Set<MovimientoDto> getMovimientoDtos() {
        return movimientoDtos;
    }

    public void setMovimientoDtos(Set<MovimientoDto> movimientoDtos) {
        this.movimientoDtos = movimientoDtos;
    }

    @Override
    public String toString() {
        return "CuentaDto{" +
                "numeroCuenta=" + numeroCuenta +
                ", moneda='" + moneda + '\'' +
                ", saldo=" + saldo +
                ", titularDto=" + titularDto +
                '}';
    }

}
