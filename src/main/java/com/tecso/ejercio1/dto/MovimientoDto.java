package com.tecso.ejercio1.dto;

import com.tecso.ejercio1.model.Cuenta;

import java.math.BigDecimal;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;

public class MovimientoDto implements Comparable<MovimientoDto> {

    private ZonedDateTime fecha;

    private String tipoMovimiento;

    private String descripcion;

    private BigDecimal importe;

    private CuentaDto cuentaDto;

    public MovimientoDto() {
        this.setFecha(ZonedDateTime.now( ZoneOffset.UTC ));
    }

    public ZonedDateTime getFecha() {
        return fecha;
    }

    public void setFecha(ZonedDateTime fecha) {
        this.fecha = fecha;
    }

    public String getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(String tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public CuentaDto getCuentaDto() {
        return cuentaDto;
    }

    public void setCuentaDto(CuentaDto cuentaDto) {
        this.cuentaDto = cuentaDto;
    }

    @Override
    public String toString() {
        return "MovimientoDto{" +
                "fecha=" + fecha +
                ", tipoMovimiento='" + tipoMovimiento + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", importe=" + importe +
                ", cuentaDto=" + cuentaDto +
                '}';
    }


    @Override
    public int compareTo(MovimientoDto o) {
        return getFecha().compareTo(o.getFecha());
    }
}
