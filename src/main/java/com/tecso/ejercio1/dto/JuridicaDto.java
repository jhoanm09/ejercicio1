package com.tecso.ejercio1.dto;

import com.tecso.ejercio1.model.Titular;

import java.io.Serializable;
import java.util.Date;

public class JuridicaDto implements Serializable {

    private String razonSocial;

    private Date anioFundacion;

    public JuridicaDto() {
    }

    public JuridicaDto(String razonSocial, Date anioFundacion) {
        this.razonSocial = razonSocial;
        this.anioFundacion = anioFundacion;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public Date getAnioFundacion() {
        return anioFundacion;
    }

    public void setAnioFundacion(Date anioFundacion) {
        this.anioFundacion = anioFundacion;
    }

    @Override
    public String toString() {
        return "JuridicaDto{" +
                "razonSocial='" + razonSocial + '\'' +
                ", anioFundacion=" + anioFundacion +
                '}';
    }
}
