package com.tecso.ejercio1.dto;

import com.tecso.ejercio1.model.Fisica;
import com.tecso.ejercio1.model.Juridica;

public class TitularDto {

    private Long cuid;

    private JuridicaDto juridicaDto;

    private FisicaDto fisicaDto;

    public TitularDto() {
    }

    public TitularDto(Long cuid, JuridicaDto juridicaDto) {
        this.cuid = cuid;
        this.juridicaDto = juridicaDto;
    }

    public TitularDto(Long cuid) {
        this.cuid = cuid;
    }

    public Long getCuid() {
        return cuid;
    }

    public void setCuid(Long cuid) {
        this.cuid = cuid;
    }

    public JuridicaDto getJuridicaDto() {
        return juridicaDto;
    }

    public void setJuridicaDto(JuridicaDto juridicaDto) {
        this.juridicaDto = juridicaDto;
    }

    public FisicaDto getFisicaDto() {
        return fisicaDto;
    }

    public void setFisicaDto(FisicaDto fisicaDto) {
        this.fisicaDto = fisicaDto;
    }

    @Override
    public String toString() {
        return "TitularDto{" +
                "cuid=" + cuid +
                ", juridicaDto=" + juridicaDto +
                ", fisicaDto=" + fisicaDto +
                '}';
    }
}
